package com.example.mruzbek.task.activity.mainActivity.di.module.context

import javax.inject.Qualifier

@Qualifier
annotation class AppContext
