package com.example.mruzbek.task.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson


class Converters {
    @TypeConverter
    fun fromString(value: String): List<String> {
        return Gson().fromJson(value, Array<String>::class.java).asList()
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}
