package com.example.mruzbek.task.di.module.context

import android.content.Context
import com.example.mruzbek.task.di.scope.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class ActivityContextModule(private val context: Context) {
    @ActivityContext
    @ActivityScope
    @Provides
    fun appContext() = context
}