package com.example.mruzbek.task.di.module

import android.content.Context
import com.example.mruzbek.task.db.TaskDatabase
import com.example.mruzbek.task.di.module.context.ActivityContext
import com.example.mruzbek.task.di.module.context.ActivityContextModule
import com.example.mruzbek.task.di.scope.ActivityScope
import com.example.mruzbek.task.ui.activity.IMainViewPresenter
import com.example.mruzbek.task.ui.activity.MainViewPresenterImpl
import dagger.Module
import dagger.Provides

@ActivityScope
@Module(includes = [ActivityContextModule::class])
class MainActivityPresenterModule {
    @ActivityScope
    @Provides
    fun getPresenter(@ActivityContext context: Context, taskDatabase: TaskDatabase): IMainViewPresenter = MainViewPresenterImpl(context, taskDatabase)

}