package com.example.mruzbek.task.aplication

import android.app.Activity
import android.app.Application
import com.example.mruzbek.task.activity.mainActivity.di.AppComponent
import com.example.mruzbek.task.activity.mainActivity.di.DaggerAppComponent
import com.example.mruzbek.task.activity.mainActivity.di.module.context.AppContextModule

class App : Application() {
    private var appComponent: AppComponent? = null

    fun component(): AppComponent {
        return appComponent!!
    }


    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appContextModule(AppContextModule(this))
                .build()


    }

    companion object {
        fun get(activity: Activity): App = activity.application as App
    }

}