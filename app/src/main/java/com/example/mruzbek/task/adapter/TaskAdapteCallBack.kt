package com.example.mruzbek.task.adapter

import android.support.v7.util.DiffUtil
import com.example.mruzbek.task.db.Task


class TaskAdapteCallBack : DiffUtil.ItemCallback<Task>() {
    override fun areItemsTheSame(oldItem: Task?, newItem: Task?): Boolean {
        return oldItem?.id == newItem?.id
    }

    override fun areContentsTheSame(oldItem: Task?, newItem: Task?): Boolean {
        return oldItem?.equals(newItem) ?: false
    }
}