package com.example.mruzbek.task.activity.mainActivity.mvp

import android.content.Context
import android.view.Gravity
import android.view.View
import com.example.mruzbek.task.R
import com.example.mruzbek.task.activity.mainActivity.mvp.view.IMainView
import com.example.mruzbek.task.db.Task
import com.example.mruzbek.task.db.TaskDatabase
import com.example.mruzbek.task.dialogs.AddTaskBottomSheetDialog
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewPresenterImpl(private val context: Context, private val dbmanager: TaskDatabase, private val addTaskBottomSheetDialog: AddTaskBottomSheetDialog) : IMainViewPresenter {


    val view by lazy {
        context as IMainView
    }
    var dialog: DialogPlus? = null

    /*  @Inject
      lateinit var dbmanager: TaskDatabase
  */
    override fun oncreate() {
        dbmanager.loadTaskDao().getAllOnFinishedTasks(false).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.adapterSubmitList(it)
                }, {})
        dbmanager.loadTaskDao().getAllOnFinishedTasks(true).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.workedListAdapterSubmitList(it)
                }, {})


    }

    override fun addTask(task: Task) {
        dbmanager.loadTaskDao().addTask(task)
        addTaskBottomSheetDialog.dismiss()

    }


    override fun deleteTask(task: Task) {
        dbmanager.loadTaskDao().deleteTask(task)
    }

    override fun updateTask(task: Task) {
        dbmanager.loadTaskDao().updateTask(task)
    }

    override fun ondestroy() {
    }

    override fun btnMenuLeftSetOnClickListener() {
        dialog = DialogPlus.newDialog(context)
                .setGravity(Gravity.TOP)
                .setCancelable(false)
                .setContentHolder(ViewHolder(R.layout.add_new_order))
                .setOnCancelListener {
                    it.dismiss()
                }
                .setOnBackPressListener {
                    it.dismiss()
                }
                .setExpanded(true, 240)
                .create()
        dialog?.show()
    }


    override fun togleButtonWorkedListSetOnClickListener(it: View) {
        var isOpen: Boolean? = null

        isOpen = if (it.tag == null) {
            true
        } else {
            it.tag as Boolean
        }
        it.tag = !isOpen
        if (isOpen) {
            view.workedTaskListShow()
        } else {
            view.workedTaskListHide()
        }
    }

    override fun onBackPressed() {
        dialog?.dismiss()
    }
}
