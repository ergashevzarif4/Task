package com.example.mruzbek.task.activity.mainActivity.di.module

import android.content.Context
import com.example.mruzbek.task.activity.mainActivity.di.module.context.ActivityContext
import com.example.mruzbek.task.activity.mainActivity.di.module.context.ActivityContextModule
import com.example.mruzbek.task.activity.mainActivity.mvp.IMainViewPresenter
import com.example.mruzbek.task.activity.mainActivity.mvp.MainViewPresenterImpl
import com.example.mruzbek.task.db.TaskDatabase
import com.example.mruzbek.task.dialogs.AddTaskBottomSheetDialog
import dagger.Module
import dagger.Provides

@ActivityScope
@Module(includes = [ActivityContextModule::class])
class MainActivityPresenterModule {
    @ActivityScope
    @Provides
    fun getPresenter(@ActivityContext context: Context, taskDatabase: TaskDatabase, dialog: AddTaskBottomSheetDialog):
            IMainViewPresenter = MainViewPresenterImpl(context, taskDatabase, dialog)

}