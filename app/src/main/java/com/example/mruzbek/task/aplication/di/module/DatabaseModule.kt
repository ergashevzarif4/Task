package com.example.mruzbek.task.activity.mainActivity.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.example.mruzbek.task.activity.mainActivity.di.module.context.AppContext
import com.example.mruzbek.task.activity.mainActivity.di.module.context.AppContextModule
import com.example.mruzbek.task.activity.mainActivity.di.scope.AppScope
import com.example.mruzbek.task.common.DATABASE_NAME
import com.example.mruzbek.task.db.TaskDatabase
import dagger.Module
import dagger.Provides

@AppScope
@Module(includes = [AppContextModule::class])
class DatabaseModule {
    @AppScope
    @Provides
    fun dbmanager(@AppContext context: Context): TaskDatabase = Room.databaseBuilder(context, TaskDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()
}