package com.example.mruzbek.task.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mruzbek.task.R

class TopSheetDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setGravity(Gravity.TOP)
        return inflater.inflate(R.layout.task_item_1, container, false)
    }
}
