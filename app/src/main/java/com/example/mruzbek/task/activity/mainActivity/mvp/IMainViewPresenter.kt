package com.example.mruzbek.task.activity.mainActivity.mvp

import android.view.View
import com.example.mruzbek.task.db.Task

interface IMainViewPresenter {
    fun oncreate()
    fun addTask(task: Task)
    fun deleteTask(task: Task)
    fun updateTask(task: Task)
    fun ondestroy()
    fun onBackPressed()
    fun btnMenuLeftSetOnClickListener()
    fun togleButtonWorkedListSetOnClickListener(it: View)


}