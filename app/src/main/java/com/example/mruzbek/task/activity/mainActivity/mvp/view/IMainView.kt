package com.example.mruzbek.task.activity.mainActivity.mvp.view

import com.example.mruzbek.task.db.Task

interface IMainView {
    fun addTask(task: Task)
    fun deleteTask(task: Task)
    fun updateTask(task: Task)
    fun adapterSubmitList(list: MutableList<Task>)
    fun workedListAdapterSubmitList(list: MutableList<Task>)

    fun workedTaskListShow()
    fun workedTaskListHide()


}