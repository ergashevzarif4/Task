package com.example.mruzbek.task.activity.mainActivity.mvp.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.mruzbek.task.R
import com.example.mruzbek.task.activity.mainActivity.di.DaggerMainAcitivityComponent
import com.example.mruzbek.task.activity.mainActivity.di.module.context.ActivityContextModule
import com.example.mruzbek.task.activity.mainActivity.mvp.IMainViewPresenter
import com.example.mruzbek.task.adapter.TaskAdapter
import com.example.mruzbek.task.aplication.App
import com.example.mruzbek.task.db.Task
import com.example.mruzbek.task.dialogs.AddTaskBottomSheetDialog
import com.orhanobut.dialogplus.DialogPlus
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.contaner_main_activity.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), IMainView {


    private val adapter = TaskAdapter()
    private val workedListAdapter = TaskAdapter()

    private var dialog: DialogPlus? = null

    var presenter: IMainViewPresenter? = null
        @Inject set

    var addTaskBottomSheetDialog: AddTaskBottomSheetDialog? = null
        @Inject set

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerMainAcitivityComponent.builder()
                .appComponent(App.get(this).component())
                .activityContextModule(ActivityContextModule(this))
                .build()
                .inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter?.oncreate()
        init()
    }

    private fun init() {
        task_list.adapter = adapter
        worked_task_list.adapter = workedListAdapter
        btn_menu_left.setOnClickListener {
            presenter?.btnMenuLeftSetOnClickListener()
        }
        btn_add_task.setOnClickListener {
            addTaskBottomSheetDialog?.show(supportFragmentManager, addTaskBottomSheetDialog?.tag)
        }
        togle_button_worked_list.setOnClickListener {
            presenter?.togleButtonWorkedListSetOnClickListener(it)
        }
    }

    override fun addTask(task: Task) {
        presenter?.addTask(task)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.ondestroy()
    }


    override fun deleteTask(task: Task) {
        presenter?.deleteTask(task)
    }

    override fun updateTask(task: Task) {
        presenter?.updateTask(task)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter?.onBackPressed()
    }

    override fun adapterSubmitList(list: MutableList<Task>) {
        adapter.submitList(list)
    }

    override fun workedTaskListHide() {
        worked_task_list.visibility = View.GONE
        togle_button_worked_list_image.setImageResource(R.drawable.ic_expand_less)

    }

    override fun workedTaskListShow() {
        worked_task_list.visibility = View.VISIBLE
        togle_button_worked_list_image.setImageResource(R.drawable.ic_expand_more)
    }

    override fun workedListAdapterSubmitList(list: MutableList<Task>) {
        workedListAdapter.submitList(list)
    }

}
