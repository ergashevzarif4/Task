package com.example.mruzbek.task.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Task {
    @PrimaryKey(autoGenerate = true)
    public Long id;
    public String title = "";
    public String message = "";
    public Long date = 1L;
    public String subtitle;
    public Boolean onFinished = false;
    public Long order_id;
}
