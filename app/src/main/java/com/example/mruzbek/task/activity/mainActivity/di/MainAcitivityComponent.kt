package com.example.mruzbek.task.activity.mainActivity.di

import com.example.mruzbek.task.activity.mainActivity.di.module.ActivityScope
import com.example.mruzbek.task.activity.mainActivity.di.module.MainActivityPresenterModule
import com.example.mruzbek.task.activity.mainActivity.mvp.view.MainActivity
import dagger.Component

@ActivityScope
@Component(modules = [MainActivityPresenterModule::class],
        dependencies = [AppComponent::class])
interface MainAcitivityComponent {
    fun inject(activity: MainActivity)
}