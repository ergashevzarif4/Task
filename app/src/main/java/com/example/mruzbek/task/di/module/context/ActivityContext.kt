package com.example.mruzbek.task.di.module.context

import javax.inject.Qualifier

@Qualifier
annotation class ActivityContext
