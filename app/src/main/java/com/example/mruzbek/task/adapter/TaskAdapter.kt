package com.example.mruzbek.task.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mruzbek.task.R
import com.example.mruzbek.task.activity.mainActivity.mvp.view.IMainView
import com.example.mruzbek.task.db.Task
import kotlinx.android.synthetic.main.task_item_3.view.*
import java.text.SimpleDateFormat
import java.util.*

class TaskAdapter : ListAdapter<Task, TaskAdapter.TaskViewHolder>(TaskAdapteCallBack()) {
    private var litener: IMainView? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_item_3, parent, false)
        litener = view.context as IMainView
        return TaskViewHolder(view)
    }


    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.onBind(getItem(position))

    }


    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val format by lazy {
            //    ju, 20 iyul
            SimpleDateFormat("EEE, d MMMMM")
        }

        fun onBind(task: Task) {
            itemView.title.text = task.title
            if (task.onFinished) {
                itemView.checkbox.setImageResource(R.drawable.ic_check)
            } else
                itemView.checkbox.setImageResource(R.drawable.ic_radio_button_unchecked)
            if (task.message != "") {
                itemView.message.text = task.message
                itemView.message.visibility = View.VISIBLE
            } else {

                itemView.message.visibility = View.GONE
            }
            if (task.date != 1L && !task.onFinished) {
                itemView.btn_calendar.text = format.format(Date(task.date))
                itemView.btn_calendar.visibility = View.VISIBLE
            } else
                itemView.btn_calendar.visibility = View.GONE
            itemView.checkbox.setTag(R.id.TASK_ID, task)

            //    itemView.checkbox.setTag(R.id.TASK_ID, task.title)

            //   itemView.checkbox.setTag(R.id.TASK_ONFINNISHED, task.onFinished)

            /* itemView.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->

                 if (isChecked &&
                         litener != null) {
                     //todo shu yerda yeb quyishim mumkin
                     litener?.deleteTask(task)
                 }
             }*/
            itemView.checkbox.setOnClickListener {
                // val task_onFineshed = it.getTag(R.id.TASK_ONFINNISHED) as Boolean
                val task = it.getTag(R.id.TASK_ID) as Task

                task.onFinished = !task.onFinished
                it.checkbox.setTag(R.id.TASK_ID, task)
                if (task.onFinished) {
                    itemView.checkbox.setImageResource(R.drawable.ic_check)
                } else
                    itemView.checkbox.setImageResource(R.drawable.ic_radio_button_unchecked)
                litener?.updateTask(task)
            }
        }

    }
}