package com.example.mruzbek.task.activity.mainActivity.di.module

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope