package com.example.mruzbek.task.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.mruzbek.task.common.DATABASE_VERSION

@Database(entities = [Task::class, Order::class], version = DATABASE_VERSION)
@TypeConverters(Converters::class)
abstract class TaskDatabase : RoomDatabase() {
    abstract fun loadTaskDao(): TaskDao
    abstract fun loadOrderDao(): OrderDao
}