package com.example.mruzbek.task.activity.mainActivity.di.module.context

import android.content.Context
import com.example.mruzbek.task.activity.mainActivity.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppContextModule(private val context: Context) {
    @AppContext
    @AppScope
    @Provides
    fun appContext() = context
}