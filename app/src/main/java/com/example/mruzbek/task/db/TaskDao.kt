package com.example.mruzbek.task.db

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface TaskDao {
    @Insert
    fun addTask(task: Task)

    @Delete
    fun deleteTask(task: Task)

    @Update
    fun updateTask(task: Task)

    @Query("SELECT * FROM Task ")
    fun getAllTasks(): Flowable<MutableList<Task>>

    @Query("SELECT * FROM Task WHERE order_id=:order_id")
    fun getOrderItems(order_id: Long): Flowable<MutableList<Task>>

    //todo shu yerda onFinished maydoni false bo`lganlarni olish kerak
    @Query("SELECT * FROM Task WHERE onFinished=:isFinished")
    fun getAllOnFinishedTasks(isFinished: Boolean): Flowable<MutableList<Task>>
}
