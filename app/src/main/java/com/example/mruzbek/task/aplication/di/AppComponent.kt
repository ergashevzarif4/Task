package com.example.mruzbek.task.activity.mainActivity.di

import com.example.mruzbek.task.activity.mainActivity.di.module.DatabaseModule
import com.example.mruzbek.task.activity.mainActivity.di.scope.AppScope
import com.example.mruzbek.task.aplication.di.module.AddTaskBottomSheetDialogModule
import com.example.mruzbek.task.db.TaskDatabase
import com.example.mruzbek.task.dialogs.AddTaskBottomSheetDialog
import dagger.Component

@AppScope
@Component(modules = [
    DatabaseModule::class,
    AddTaskBottomSheetDialogModule::class
])
interface AppComponent {
    val dbmanger: TaskDatabase
    val dialog: AddTaskBottomSheetDialog
}