package com.example.mruzbek.task.db

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface OrderDao {
    @Insert
    fun addOrder(order: Order)

    @Delete
    fun deleteOrder(order: Order)

    @Update
    fun updateOrder(order: Order)

    @Query("SELECT * FROM  `Order` ")
    fun getAllOrders(): Flowable<MutableList<Order>>
}