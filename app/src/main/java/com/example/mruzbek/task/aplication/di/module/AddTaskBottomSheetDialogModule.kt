package com.example.mruzbek.task.aplication.di.module

import com.example.mruzbek.task.activity.mainActivity.di.scope.AppScope
import com.example.mruzbek.task.dialogs.AddTaskBottomSheetDialog
import dagger.Module
import dagger.Provides

@AppScope
@Module
class AddTaskBottomSheetDialogModule {
    @AppScope
    @Provides
    fun getDialog() = AddTaskBottomSheetDialog()

}