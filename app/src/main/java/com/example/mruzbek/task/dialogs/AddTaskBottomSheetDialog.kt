package com.example.mruzbek.task.dialogs

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.example.mruzbek.task.R
import com.example.mruzbek.task.activity.mainActivity.mvp.view.IMainView
import com.example.mruzbek.task.db.Task
import kotlinx.android.synthetic.main.add_task_bottom_sheet_layout.view.*

class AddTaskBottomSheetDialog : BottomSheetDialogFragment() {
    var addTaskListener: IMainView? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is IMainView)
            addTaskListener = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.add_task_bottom_sheet_layout, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.btn_save_task.setOnClickListener {

            val task = Task()
            task.title = view.et_task_title.text.toString()
            task.onFinished = false
            addTaskListener?.addTask(task)


        }
        view.add_task_btn_func.setOnClickListener {
            var isopen = false
            if (it.tag != null) {
                isopen = it.tag as Boolean

            }
        }
        //@SuppressLint("RestrictedApi")
        // override fun setupDialog(dialog: Dialog?, style: Int) {
        //super.setupDialog(dialog, style)
        /*val contentView = View.inflate(context, R.layout.add_task_bottom_sheet_layout, null)
        dialog?.setContentView(contentView)
        */dialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        //    getDialog().window.setSoftInputMode(WindowManager.LayoutParams.)

        /* contentView.btn_save_task.setOnClickListener {

             val task = Task()
             task.title = contentView.et_task_title.text.toString()
             task.onFinished = false
             addTaskListener?.addTask(task)
 */
        //}

    }
}