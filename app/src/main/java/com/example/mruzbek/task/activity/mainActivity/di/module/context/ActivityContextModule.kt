package com.example.mruzbek.task.activity.mainActivity.di.module.context

import android.content.Context
import com.example.mruzbek.task.activity.mainActivity.di.module.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class ActivityContextModule(private val context: Context) {
    @ActivityContext
    @ActivityScope
    @Provides
    fun appContext() = context
}